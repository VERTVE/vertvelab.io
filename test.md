---
layout: default
title: test
permalink: /test/
---

<div>
  <h4><b>Codifying your malware analysis workflows.</b></h4>
  <p>
    Building shareable, repeatable & history preserving analysis pipelines
    using your favourite tools + CI + git + containers.
  </p>
  <p>
    Analysis tools, that will run automatically whenever possible,
    and the results are automatically evaluated and compiled into
    shareable threat intelligence packages.
  </p>
</div>

<br>
<b>Key features:</b>
<br>
<table class="tg">
  <tr>
    <th><img src="/public/includes/terminal.png" alt="Tools icon" /></th>
    <th><img src="/public/includes/terminal.png" alt="Tools icon" /></th>
    <th><img src="/public/includes/terminal.png" alt="Tools icon" /></th>
    <th><img src="/public/includes/terminal.png" alt="Tools icon" /></th>
  </tr>
  <tr>
    <td>TOOLS</td>
    <td>CINCAN-COMMAND</td>
    <td>MINION</td>
    <td>PIPELINES</td>
  </tr>
  <tr>
    <td>Dockerized analysis tools</td>
    <td>Run native command-line tools provided as docker images</td>
    <td>Build analysis pipelines using command-line tools and minion rules</td>
    <td>Try our analysis environment built upon Concourse CI pipelines and Gitlab</td>
  </tr>
</table>


| Key features |          |                     |          |
|--------------|----------|---------------------|----------|
|  ![tools](/public/includes/terminal.png)  | ![tools](/public/includes/face20.png) | ![tools](/includes/face20.png)| ![tools](/includes/face20.png) |
| TOOLS | CINCAN-COMMAND    | MINION    | PIPELINES |



!!! note "[Try it out now!](https://cincan.io/environment/installation/)"
Easy to install environment as docker-compose.


{% capture my_include %}{% include tools-readme.md %}{% endcapture %}
{{ my_include | markdownify }}


| Tool name                | Description                                        | Input               | Platform |
|--------------------------|----------------------------------------------------|---------------------|----------|
| access-log-visualization | Visualizing webserver's access log data            | access.log (Apache) | Linux    |
| binary-analysis-tool-bat | Binary Analysis Tool BAT with extra tools          | binary              | Linux    |
| binwalk                  | Firmware analysis tool                             | binary              | Linux    |
| c-ci                     | The Concourse CI                                   |                     | Linux    |
| c-worker                 | The Concourse Worker                               |                     | Linux    |
| clamscan                 | ClamAV virus scanner                               | any                 | Linux    |
| dns-tools                |                                                    |                     | Linux    |
| dotnetdecompile          | Decompiler using th ILSpy engine                   | .NET                | Linux    |
| flawfinder               | Scan C/C++ code for security flaws                 | C/C++               | Linux    |
| ghidra-decompiler        |                                                    |                     | Linux    |
| hyperscan                | Regular expression matching library                |                     | Linux    |
| identify-file            | Identifies file type using several techniques      |                     | Linux    |
| iocextract               | Extracts urls, hashes, emails and ips from a file  | any                 | Linux    |
| jsunpack-n               | Scans URLs and PDFs.                               | PDF,URL,PCAP,JS,SWF | Linux    |
| keyfinder                | Find and analyze key files on a filesystem or APK  | filesystems, APK    | Linux    |
| manalyze                 | A Static analyzer for executables                  | binary              | Linux    |
| oledump-linux            | Analyze MS Office (OLE) files                      | DOC, XLS, PPT       | Linux    |
| olevba                   | Extract and analyze VBA macros from Office files   | DOC,DOT,XML,PPTM,VBA| Linux    |
| pdf-tools                | The DidierStevensSuite                             | PDF                 | Linux    |
| pdf2john                 | John The Ripper for extracting hash from PDF       | Encrypted PDF       | Linux    |
| pdfexaminer              | Uploads PDF to www.pdfexaminer.com for scanning    | PDF                 | Linux    |
| pdfid                    | Scan PDFs for keywords, Javascript, auto-open...   | PDF                 | Linux    |
| pdfxray_lite             | Analyze PDFs                                       | PDF                 | Linux    |
| pe-scanner               |                                                    |                     | Linux    |
| peepdf                   | A Powerful python tool to analyze PDFs             | PDF, shellcode      | Linux    |
| pywhois                  | Retrieve information of IP addresses               | IP                  | Linux    |
| r2-bin-carver            | A Script to carve files from memory dumps          | memory dumps        | Linux    |
| r2-callgraph             | Modificated Radare2 image to analyze binaries      | ELF / PE binary     | Linux    |
| rtfobj                   | Detect and extract (OLE) objects in RTF files      | RTF                 | Linux    |
| s3-resource-simple       | A Resource to upload files to S3                   |                     | Linux    |
| scrape-website           |                                                    |                     | Linux    |
| shellcode2exe            | Converts shellcode into executables                | shellcode           | Linux    |
| sleuthkit                | Open source digital forensics                      |                     | Linux    |
| snowman-decompile        | A Native code to C/C++ decompiler                  | ELF Mach-O PE LE    | Linux    |
| suricata                 | Network threat detection engine (IDS, IPS, NSM)    | PCAP,network traffic| Linux    |
| trufflehog               | Search git repos for accidentally committed secrets| git repository      | Linux    |
| tshark                   | Parse PCAP and capture network traffic             | PCAP,network traffic| Linux    |
| twiggy                   | Analyze a binary's call graph                      | .wasm (ELF/Mach-O)  | Linux    |
| vba2graph                | Generate call graphs from VBA code                 | DOC,XLS,BAS         | Linux    |
| virustotal               | Analyze files and URLs to detect malware           | any                 | Linux    |
| volatility               | An Advanced memory forensics framework             | memory samples      | Linux    |
| xmldump                  | Parse XML files                                    | XML                 | Linux    |
| Windows tools            |                                                    |                     |          |
|--------------------------|----------------------------------------------------|---------------------|----------|
| binskim                  | A Light-weight PE scanner                          | binary              | Windows  |
| capture-bat              | A Behavioral analysis tool of WIN32 apps           |                     | Windows  |
| convertshellcode         | Shellcode disassembler                             | shellcode           | Windows  |
| jakstab                  |                                                    |                     | Windows  |
| officemalscanner         | Scan MS Office files for malicious traces          | DOC, XLS, PPT       | Windows  |
| oledump                  | Analyze MS Office (OLE) files                      | DOC, XLS, PPT       | Windows  | 
| pdf-parser               | Parse PDF to identify the fundamental elements     | PDF                 | Windows  |
| pdf-stream-dumper        | Analyze PDF documents                              | PDF                 | Windows  |
| pestudio                 | Scan binary files for security related information | EXE,DLL,CPL,OCX...  | Windows  |
| processhacker            | Monitor resources, debug software, detect malware  |                     | Windows  |
| regshot                  | Registry compare utility                           |                     | Windows  |
| sysanalyzer              | An Automated malcode runtime analysis application  |                     | Windows  |
| sysinternals             | Manage, troubleshoot and diagnose Win systems/apps |                     | Windows  |
